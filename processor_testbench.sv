`timescale 1ns/1ps
`include "common.sv"

/* Simple testbench for processor
** 
**/
module processor_testbench();
	logic [1:0] symbol;
	logic clk;
	logic reset;
	logic out;
	logic valid;
	logic valid_in;
	integer f;
	
	logic next_symbol;
	logic [1:0] symbols [0:119] = {2'h0, 2'h3, 2'h2, 2'h0, 2'h1, 2'h0, 2'h0, 2'h3, 2'h0, 2'h3, 2'h0, 2'h3, 2'h0, 2'h3, 2'h0, 2'h3, 2'h0, 2'h3, 2'h0, 2'h3, 2'h0, 2'h3, 2'h0, 2'h3, 2'h0, 2'h3, 2'h0, 2'h3, 2'h0, 2'h3, 2'h0, 2'h3, 2'h0, 2'h3, 2'h0, 2'h3, 2'h0, 2'h3, 2'h0, 2'h3, 2'h0, 2'h3, 2'h0, 2'h3, 2'h0, 2'h3, 2'h0, 2'h3, 2'h0, 2'h3,
 2'h0, 2'h3, 2'h0, 2'h3, 2'h0, 2'h3, 2'h0, 2'h3, 2'h0, 2'h3, 2'h0, 2'h3, 2'h0, 2'h3, 2'h0, 2'h3, 2'h0, 2'h3, 2'h0, 2'h3, 2'h0, 2'h3, 2'h0, 2'h3, 2'h0, 2'h3, 2'h0, 2'h3, 2'h0, 2'h3, 2'h0, 2'h3, 2'h0, 2'h3, 2'h0, 2'h3, 2'h0, 2'h3, 2'h0, 2'h3, 2'h0, 2'h3, 2'h0, 2'h3, 2'h0, 2'h3, 2'h0, 2'h3, 2'h0, 2'h3,
 2'h0, 2'h3, 2'h0, 2'h3, 2'h0, 2'h3, 2'h0, 2'h3, 2'h0, 2'h3, 2'h0, 2'h3, 2'h0, 2'h3, 2'h0, 2'h0, 2'h2, 2'h3, 2'h1, 2'h3};
 
 /*{2'h3, 2'h1, 2'h2, 2'h1, 2'h1, 2'h0, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3,
 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3,
 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h3, 2'h0, 2'h2, 2'h1, 2'h2, 2'h2, 2'h3};*/
	integer count = 0;
	integer endcount = 0;

	processor U0 (
		.clk				(clk),
		.symbol			(symbol), 
		.out				(out),
		.valid_in		(valid_in),
		.async_reset_n	(reset),
		.valid_out		(valid),
		.next_symbol	(next_symbol)
	); 
	
	lifo_buffer lifo (
		.clk				(clk),
		.reset_n			(reset),
		.en				(valid),
		.data				(out)	
	);
	
	
	integer i;
	
	always begin
			#50 clk = ~clk; 
			#50 clk = ~clk; 
	end
	
	initial begin 
		clk = 1'b0;
		reset = 0;
		valid_in = 0;
		count = 0;
		symbol = 2'bxx;
		#200 reset = 1;
	end 
	
	always @(posedge clk) begin
		if (next_symbol) begin
			if (count < 120) begin
				symbol = symbols[count];
				valid_in = 1;
			end else begin
				valid_in = 0;
			end
			count = count + 1;
		end
		
		if (count < 120) endcount = 0;
		else endcount += 1;
		
		if (endcount == 2048) $stop;
	end


endmodule