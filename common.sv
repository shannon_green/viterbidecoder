// Constraint length
`define PARAM_K 7

// Rate = 1/R
`define PARAM_R 2

// Total number of states
`define NUM_STATES (1<<(`PARAM_K-1))

// Size of branch metric
`define WIDTH_BM ($clog2(`PARAM_R+1))

// Size of path/state metric
`define WIDTH_PM 8

// Size of a state index
`define WIDTH_STATE (`PARAM_K-1)

// Generator polynomials (g_0, g_1)
`define GENERATOR_POLYNOMIALS ('{7'o171, 7'o133})

`define TBLEN 40

`define INPUT_LEN 240