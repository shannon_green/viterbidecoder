`include "common.sv"

/* ACS (add-compare-select) unit (dual)
** Inputs: bm00, bm01, bm10, bm11, oldsm0, oldsm1
** Outputs: newsm0, newsm1, dec
**/
module acs 
	(
		input logic clk,
		input logic valid_in,
	
		// Input branch metric
		input logic [`WIDTH_BM-1:0] bm00,
		input logic [`WIDTH_BM-1:0] bm01,
		input logic [`WIDTH_BM-1:0] bm10,
		input logic [`WIDTH_BM-1:0] bm11,
		
		// Old state metrics
		input logic [`WIDTH_PM-1:0] oldsm0,
		input logic [`WIDTH_PM-1:0] oldsm1,
		
		// New state metrics
		output logic [`WIDTH_PM-1:0] newsm0,
		output logic [`WIDTH_PM-1:0] newsm1,
		
		// Decision bits from select operation
		output logic [1:0] dec,
		
		output logic valid_out
	);
	
	// Path metrics for all branches
	logic [`WIDTH_PM-1:0] pm00, pm01, pm10, pm11;
	logic is_inf_0, is_inf_1;
	
	logic [1:0] dec_out;
	logic [`WIDTH_PM-1:0] newsm0_out;
	logic [`WIDTH_PM-1:0] newsm1_out;
	
	always_comb begin
		// Check if any state metrics are at infinity.
		is_inf_0 = (oldsm0 == {`WIDTH_PM{{1'b1}}});
		is_inf_1 = (oldsm1 == {`WIDTH_PM{{1'b1}}});
	
		// Add
		// Don't do the addition if a state metric is at infinity -
		// that would result in an overflow. 
		pm00 = is_inf_0 ? oldsm0 : oldsm0 + bm00;
		pm01 = is_inf_1 ? oldsm1 : oldsm1 + bm10;
		pm10 = is_inf_0 ? oldsm0 : oldsm0 + bm01;
		pm11 = is_inf_1 ? oldsm1 : oldsm1 + bm11;
		
		// Compare
		dec_out = {(pm00 > pm01), (pm10 > pm11)};
		
		// Select
		newsm0_out = dec_out[1] ? pm01 : pm00;
		newsm1_out = dec_out[0] ? pm11 : pm10;
	end
	
	assign {newsm0, newsm1} = {newsm0_out, newsm1_out};
	assign valid_out = valid_in;
	
	always_ff @(posedge clk) begin
		dec <= dec_out;
		//{newsm0, newsm1} <= {newsm0_out, newsm1_out};
		
		//valid_out <= valid_in;
	end
		
endmodule
