`include "common.sv"

module metric_memory
	(
		input logic we, 
		input logic clk,
		input logic [`WIDTH_STATE-1:0] read_state_counter, 
		input logic [`WIDTH_STATE-1:0] write_state_counter, 
		input logic [`WIDTH_PM-1:0] read_stage_counter,
		input logic [`WIDTH_PM-1:0] write_stage_counter,
		input logic [`WIDTH_PM-1:0] newsm0,
		input logic [`WIDTH_PM-1:0] newsm1,
		output logic [`WIDTH_PM-1:0] oldsm0,
		output logic [`WIDTH_PM-1:0] oldsm1
	);
	
	// State metric memory
	// Effectively a 3-port RAM.
	// Width is doubled to allow writing of two state 
	// metrics in a single cycle.
	//logic [2*`WIDTH_PM-1:0] smm0 [`NUM_STATES-1:0];
	//logic [2*`WIDTH_PM-1:0] smm1 [`NUM_STATES-1:0];
	
	logic [`WIDTH_STATE-1:0] read_state0;
	logic [`WIDTH_STATE-1:0] read_state1;
	logic [`WIDTH_STATE-1:0] write_state0;
	
	//logic [`WIDTH_STATE-1:0] write_state1;
	
	logic [`WIDTH_STATE-1:0] oldsm0_index;
	logic [`WIDTH_STATE-1:0] oldsm1_index;
	
	logic [2*`WIDTH_PM-1:0] read_sm0;
	logic [2*`WIDTH_PM-1:0] read_sm1;
	
	always_comb begin
		write_state0 = write_state_counter | ((~write_stage_counter[0]) << (`PARAM_K-2));
		//write_state1 = write_state_counter | (`NUM_STATES >> 1);	
		read_state0 = (read_state_counter << 1);
		read_state1 = (read_state_counter << 1) | 1;
		
		oldsm0_index = (read_state0 & ~(6'b1 << (`PARAM_K-2)))
						| (read_stage_counter[0] << (`PARAM_K-2)); 
						
		oldsm1_index = (read_state1 & ~(6'b1 << (`PARAM_K-2)))
						| (read_stage_counter[0] << (`PARAM_K-2)); 
						
		if (write_stage_counter == 0) begin
			oldsm0 = (write_state_counter == 0) ? 1'b0 : -1;
			oldsm1 = -1;
		end else begin				
			oldsm0 = ((write_state_counter << 1) & (6'b1 << (`PARAM_K-2))) ? 
								read_sm0[`WIDTH_PM-1:0] : 
								read_sm0[2*`WIDTH_PM-1:`WIDTH_PM];
								
			oldsm1 = (((write_state_counter << 1) | 1) & (6'b1 << (`PARAM_K-2))) ? 
								read_sm1[`WIDTH_PM-1:0] : 
								read_sm1[2*`WIDTH_PM-1:`WIDTH_PM];					
		end
	end

	
	/*ram #(
		.DATA_WIDTH(2*`WIDTH_PM),
		.LENGTH(`NUM_STATES),
		.K(`PARAM_K)
	) smm0 (
		.clock		(clk),
		.data			({newsm0, newsm1}),
		.rdaddress	(oldsm0_index),
		.wraddress	(write_state0),
		.wren			(we),
		.q				(read_sm0)
	);
	
	ram #(
		.DATA_WIDTH(2*`WIDTH_PM),
		.LENGTH(`NUM_STATES),
		.K(`PARAM_K)
	) smm1 (
		.clock		(clk),
		.data			({newsm0, newsm1}),
		.rdaddress	(oldsm1_index),
		.wraddress	(write_state0),
		.wren			(we),
		.q				(read_sm1)
	);*/
	
	sync_ram #(
		.DATA_WIDTH(2*`WIDTH_PM),
		.NUMWORDS(`NUM_STATES)
	) smm0 (
		.clk		(clk),
		.data			({newsm0, newsm1}),
		.rdaddress	(oldsm0_index),
		.wraddress	(write_state0),
		.wren			(we),
		.q				(read_sm0)
	);
	
	sync_ram #(
		.DATA_WIDTH(2*`WIDTH_PM),
		.NUMWORDS(`NUM_STATES)
	) smm1 (
		.clk		(clk),
		.data			({newsm0, newsm1}),
		.rdaddress	(oldsm1_index),
		.wraddress	(write_state0),
		.wren			(we),
		.q				(read_sm1)
	);
   
endmodule
