`timescale 1ns/1ps
`include "common.sv"

/* Simple testbench for LIFO buffer
** 
**/
module lifo_testbench();
	
	logic clk;
	logic reset_n;
	logic data_in;
	logic [7:0] data_out;
	logic en;
	logic data_ready;
	
	lifo_buffer lifo
	(
		.clk			(clk),
		.reset_n		(reset_n),
		.data			(data_in),
		.en			(en),
		.data_out	(data_out),
		.data_ready	(data_ready)
	);
	
	integer i;
	
	initial begin 
		clk = 1'b0;
		reset_n = 1'b1;
		en = 0;
		#50;
		
		reset_n = 1'b0;
		
		#50 clk = 1;
		#50 clk = 0;
		#50 clk = 1;
		
		reset_n = 1'b1;
		data_in = 1;
		#50 clk = 0;
		#50 clk = 1;
		
		en = 1;		
		for (i = 0; i < 129; i++) begin
			data_in = data_in ^ 1;
			if (i >= 120) en = 0;
			#50 clk = 0;
			#50 clk = 1;
		end
		
		$stop;
	end 

endmodule