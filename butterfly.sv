`include "common.sv"

/* Butterfly processing unit
** Inputs: clk, symbol, state0, state1, oldsm0, oldsm1
** Outputs: newsm0, newsm1, dec
** Encompasses two pipeline registers.
**/

module butterfly 
	(
		input logic clk,
		input logic valid_in,
		input logic [`PARAM_R-1:0] symbol,
		input logic [`WIDTH_STATE-1:0] state0,
		input logic [`WIDTH_STATE-1:0] state1,
		input logic [`WIDTH_PM-1:0] oldsm0,
		input logic [`WIDTH_PM-1:0] oldsm1,
		output logic [`WIDTH_PM-1:0] newsm0,
		output logic [`WIDTH_PM-1:0] newsm1,
		output logic [1:0] dec,
		output logic valid_out
	);
	
	logic [`WIDTH_BM-1:0] bm00, bm01, bm10, bm11;
	logic [`WIDTH_BM-1:0] bm00_1, bm01_1, bm10_1, bm11_1;
	logic valid_bmu, valid_bmu_1;
	
	logic [`WIDTH_PM-1:0] oldsm0_1;
	logic [`WIDTH_PM-1:0] oldsm1_1;
	
	// Branch metric units
	bmu #(.PATH_SELECT(0)) bmu00  (
		.clk				(clk),
		.symbol			(symbol),
		.state			(state0),
		.out				(bm00)
	);
	
	bmu #(.PATH_SELECT(1)) bmu01 (
		.clk				(clk),
		.symbol			(symbol),
		.state			(state0),
		.out				(bm01)
	);
	
	bmu #(.PATH_SELECT(0)) bmu10 (
		.clk				(clk),
		.symbol			(symbol),
		.state			(state1),
		.out				(bm10)
	);
	
	bmu #(.PATH_SELECT(1)) bmu11 (
		.clk				(clk),
		.symbol			(symbol),
		.state			(state1),
		.out				(bm11)
	);
	
	// ACS unit
	acs acs0 (
		.clk			(clk),
		.valid_in	(valid_bmu_1),
		.valid_out  (valid_out),
		
		.bm00			(bm00_1),
		.bm01			(bm01_1),
		.bm10			(bm10_1),
		.bm11			(bm11_1),
		.oldsm0		(oldsm0),
		.oldsm1		(oldsm1),
		.newsm0		(newsm0),
		.newsm1		(newsm1),
		.dec			(dec)
	);
	
	
	always_ff @(posedge clk) begin
		{bm00_1, bm01_1, bm10_1, bm11_1} <= {bm00, bm01, bm10, bm11};
		//{oldsm0_1, oldsm1_1} <= {oldsm0, oldsm1};
		
		valid_bmu <= valid_in;
		valid_bmu_1 <= valid_bmu;
		
	end
endmodule