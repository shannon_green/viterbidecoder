`timescale 1ns/1ps
`include "common.sv"

/* Simple testbench for top level viterbi decoder 
** 
**/
module viterbi_testbench();
	logic clk;
	logic reset_n;
	logic chipselect;
	logic write;
	logic read;
	logic [11:0] address;
	logic [7:0] writedata;
	logic [7:0] readdata;
	
	logic [7:0] data [0:29] = {8'h2c, 8'hc1, 8'hcc, 8'hcc, 8'hcc, 8'hcc, 8'hcc, 8'hcc, 8'hcc, 8'hcc, 8'hcc, 8'hcc, 8'hcc, 8'hcc, 8'hcc, 8'hcc, 8'hcc, 8'hcc, 8'hcc, 8'hcc, 8'hcc, 8'hcc, 8'hcc, 8'hcc, 8'hcc, 8'hcc, 8'hcc, 8'hcc, 8'hc, 8'hde};
	integer i;
	integer t;
	ViterbiDecoder U0 (.*); 
	
	always begin
			#50 clk = ~clk; 
			#50 clk = ~clk; 
	end
	
	initial begin 
		clk = 1'b0;
		read = 0; 
		chipselect = 0;
		write = 0;
		writedata = 0;
		address = 0;
		reset_n = 0;

		#200 reset_n = 1;
		
		chipselect = 1;
		// set up address for writing data
		#200;
		
		address = {2'h0, {10{{1'b0}}}};
		i = 0;
		write = 1;
		
		// write to input buffer
		while(i < 30) begin
			writedata = data[address];
			write = 1;
			#100;
			address = address + 1;
			i = i + 1;
		end
		write = 0;
		#100;
		
		// Tell decoder to start
		address = {2'h2, {10{{1'b0}}}};
		write = 1;
		#100;
		write = 0;
		t = 0;
		address = {2'h2, {10{{1'b0}}}};
		
		while(!t) begin
			read = 1;
			#100;
			#100;
			#100;
			#100;
			t = readdata;
			read = 0;
			#100;
		end
		
		// Now soft reset and go again
		address = {2'h3, {10{{1'b0}}}};
		write = 1;
		#100;
		write = 0;
		#100;
		
		chipselect = 1;
		// set up address for writing data
		#200;
		
		address = {2'h0, {10{{1'b0}}}};
		i = 0;
		write = 1;
		
		// write to input buffer
		while(i < 30) begin
			writedata = 8'hff;
			write = 1;
			#100;
			address = address + 1;
			i = i + 1;
		end
		write = 0;
		#100;
		
		// Tell decoder to start
		address = {2'h2, {10{{1'b0}}}};
		write = 1;
		#100;
		write = 0;
		t = 0;
		address = {2'h2, {10{{1'b0}}}};
		
		while(!t) begin
			read = 1;
			#100;
			#100;
			#100;
			#100;
			t = readdata;
			read = 0;
			#100;
		end
		
		
		$stop;
	end 
	

endmodule