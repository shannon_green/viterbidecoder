/* Mixed-width synchronous RAM module with one read and one write port.
 * ASSUMES that the read port has a larger width than the write port.
 * Inputs data, rdaddress, wraddress, wren are registered.
 * Output q is registered.
 * Read-while-write behaviour is undefined.
 */
module sync_ram_mixed #(
		parameter DATA_WIDTH_READ = 8,
		parameter DATA_WIDTH_WRITE = 1,
		parameter NUMBITS = 64
	)
	(
		input logic clk,
		input logic [DATA_WIDTH_WRITE-1:0] data,
		input logic [$clog2(NUMBITS/DATA_WIDTH_READ)-1:0] rdaddress,
		input logic [$clog2(NUMBITS/DATA_WIDTH_WRITE)-1:0] wraddress,
		input logic wren,
		output logic [DATA_WIDTH_READ-1:0] q
	);
	

	localparam DATA_WIDTH_MAX = DATA_WIDTH_READ > DATA_WIDTH_WRITE ? DATA_WIDTH_READ : DATA_WIDTH_WRITE;
	localparam DATA_WIDTH_MIN = DATA_WIDTH_READ > DATA_WIDTH_WRITE ?  DATA_WIDTH_WRITE : DATA_WIDTH_READ;
	localparam RATIO = DATA_WIDTH_MAX / DATA_WIDTH_MIN;
	
	// Memory
	logic [RATIO-1:0][DATA_WIDTH_MIN-1:0] mem [0:(NUMBITS/DATA_WIDTH_MAX)-1] /* synthesis ramstyle = "no_rw_check" */;
	
	// Internal signals
	logic [$clog2(NUMBITS/DATA_WIDTH_READ)-1:0] rdaddress_d;
	
	// Register read address input
	always_ff @(posedge clk) begin
		rdaddress_d <= rdaddress;
	end
	
	// Read/write data
	
	generate if (DATA_WIDTH_READ > DATA_WIDTH_WRITE) begin
		always_ff @(posedge clk) begin
				if (wren) begin
					mem[wraddress / RATIO][wraddress % RATIO] <= data;
				end
				q <= mem[rdaddress_d];
		end
		
	end else begin
		always_ff @(posedge clk) begin
			if (wren) begin
				mem[wraddress] <= data;
			end
			q <= mem[rdaddress_d / RATIO][rdaddress_d % RATIO];
		end
	end endgenerate
	
endmodule