`include "common.sv"

module processor
	(
		input logic clk,
		input logic reset_n,
		input logic [`PARAM_R-1:0] symbol,
		input logic valid_in,
		output logic out,
		output logic next_symbol,
		output logic valid_out
	);
	
	logic [`WIDTH_STATE-1:0] in_state0;
	logic [`WIDTH_STATE-1:0] in_state1;
	
	logic [`WIDTH_STATE-1:0] state_counter = 0;
	logic [`WIDTH_STATE-1:0] state_counter_1;
	logic [`WIDTH_STATE-1:0] state_counter_2;
	logic [`WIDTH_STATE-1:0] state_counter_3;
	
	logic [`WIDTH_PM-1:0] stage_counter = 0;
	logic [`WIDTH_PM-1:0] stage_counter_1;
	logic [`WIDTH_PM-1:0] stage_counter_2;
	
	logic [1:0] dec;
	
	logic [`WIDTH_PM-1:0] oldsm0;
	logic [`WIDTH_PM-1:0] oldsm1;
	logic [`WIDTH_PM-1:0] newsm0;
	logic [`WIDTH_PM-1:0] newsm1;

	logic acs_valid;

//	// Synchronous reset
//	logic reset_n, rff;
//	
//	// Reset synchroniser
//	always_ff @(posedge clk or negedge async_reset_n) begin
//		if (!async_reset_n) {reset_n, rff} <= 2'b0;
//		else {reset_n, rff} <= {rff, 1'b1};
//	end

	
	butterfly butt0 (
		.clk			(clk),
		.valid_in	(valid_in),
		.state0		(in_state0),
		.state1		(in_state1),
		.dec			(dec),
		.oldsm0		(oldsm0),
		.oldsm1		(oldsm1),
		.newsm0		(newsm0),
		.newsm1		(newsm1),
		.symbol		(symbol),
		.valid_out	(acs_valid)
	);
	
	metric_memory smm 
	(
		.we						(acs_valid),
		.clk						(clk),
		.read_state_counter	(state_counter), 
		.write_state_counter	(state_counter_2), 
		.read_stage_counter	(stage_counter),
		.write_stage_counter	(stage_counter_2),
		.newsm0					(newsm0),
		.newsm1					(newsm1),
		.oldsm0					(oldsm0),
		.oldsm1					(oldsm1)
	);
	
	survivor_memory smu
	(
		.clk				(clk),
		.reset_n			(reset_n),
		.state_counter	(state_counter_3),
		.dec				(dec),
		.valid			(acs_valid),
		.filo_enable	(valid_out),
		.out				(out)
	);
	
	always_comb begin
		in_state0 = 2 * state_counter;
		in_state1 = 2 * state_counter + 1;
	end
	

	always_ff @(posedge clk) begin: process_stage
		if (!reset_n) begin
			// Invalidate everything
			state_counter <= 0;
			stage_counter <= 0;
			next_symbol <= 0;	
			
		end else begin
			if (valid_in) begin
				if (state_counter == (`NUM_STATES >> 1) - 1) begin
					state_counter <= 0;
					stage_counter <= stage_counter + 1'b1;
				end else begin
					state_counter <= state_counter + 1'b1;
				end
				
				if (state_counter == (`NUM_STATES >> 1) - 2) begin
					next_symbol <= 1'b1;
				end else begin
					next_symbol <= 0;
				end
				
			end else begin
				state_counter <= 0;
				stage_counter <= 0;
				next_symbol <= ~next_symbol;
			end
			
			
			// State counter pipeline
			state_counter_1 <= state_counter;
			state_counter_2 <= state_counter_1;
			state_counter_3 <= state_counter_2;
			
			// Stage counter pipeline
			stage_counter_1 <= stage_counter;
			stage_counter_2 <= stage_counter_1;	

		end
	end
	
endmodule