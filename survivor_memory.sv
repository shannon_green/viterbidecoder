`include "common.sv"
 
 module survivor_memory
	(
		input logic clk,
		input logic reset_n,
		input logic [`WIDTH_STATE-1:0] state_counter,
		input logic [`PARAM_R-1:0] dec,
		input logic valid,
		output logic out,
		output logic filo_enable
	);
	
	// SMU states
	// Quartus requires that this is declared as unsigned
	enum int unsigned {IDLE, WRITE_1, WRITE_2, WTRACEBACK, WDECODE, WIDLE, TRACEBACK_ONLY, DECODE_ONLY, DECODE_FINISH} smu_state, smu_state_next;
	
	logic [0:`NUM_STATES-1] dec_write_buffer;
	logic [0:`NUM_STATES-1] dec_read_buffer;
	
	logic block_complete;
	logic last_state;
	logic tb_decode_complete;
	
	logic [6:0] read_counter;
	logic [6:0] write_pointer;
	logic [6:0] read_pointer;
	logic [6:0] read_base;
	logic [6:0] decode_bit;
	
	logic do_write;
	logic do_traceback;
	logic do_decode;
	logic finish;
	logic idle;
	
	logic wren;
	
	logic stall_0, stall_1, stall_2, stall_3;
		
	sync_ram #(
		.DATA_WIDTH	(`NUM_STATES),
		.NUMWORDS	(`TBLEN*3)
	) smu0 (
		.clk			(clk),
		.data			(dec_write_buffer),
		.rdaddress	(read_pointer),
		.wraddress	(write_pointer),
		.wren			(wren),
		.q				(dec_read_buffer)
	);

	always_comb begin
		block_complete = (write_pointer == `TBLEN-1) || 
							  (write_pointer == 2*`TBLEN-1) || 
							  (write_pointer == 3*`TBLEN-1);
		last_state = (state_counter == `NUM_STATES/2-1);
		tb_decode_complete = (read_counter == `TBLEN-1);
		
		read_pointer = read_base + `TBLEN - 1 - read_counter;
		
		filo_enable = (stall_2 | stall_3) & do_decode;
	end
	
	// Produce delayed signals
	always_ff @(posedge clk) begin
		stall_3 <= stall_2;
		stall_2 <= stall_1;
		stall_1 <= stall_0;
	end
	
	// Traceback/decode operation
	always_ff @(posedge clk) begin
		if (!reset_n) begin
			read_counter <= 0;
			decode_bit <= 0;
			stall_0 <= 0;
			read_base <= `TBLEN;
		end else begin
			if (do_traceback || do_decode) begin				
				if (tb_decode_complete) begin
					if (!stall_3) begin // if finished stalling
					
						if (smu_state == DECODE_ONLY) begin
							case (read_base)
								`TBLEN: 		read_base <= `TBLEN*2;
								`TBLEN*2:	read_base <= 0;
								default:		read_base <= `TBLEN;
							endcase
						end else begin 
							case (read_base)
								`TBLEN: 		read_base <= 0;
								`TBLEN*2:	read_base <= `TBLEN;
								default:		read_base <= `TBLEN*2;
							endcase
						end
						read_counter <= 0;
					end
					
					stall_0 <= 1'b0;
					
				end else begin
					read_counter <= read_counter + 1;
				end
			end else begin
				read_counter <= 0;
			end
			
			// Traceback and final decode have to start from state 0
			if ((read_counter == 0) && (do_traceback || do_decode)) begin
				stall_0 <= 1'b1;
			end
				
			if (stall_0 || stall_1) begin
				if (!stall_1 && (finish || do_traceback)) begin
					decode_bit <= 0;
				end else begin
					// The traceback operation is really this line!
					decode_bit <= ((decode_bit << 1) | dec_read_buffer[decode_bit]) & {6{1'b1}};
					
				end
				
			end
			out <= dec_read_buffer[decode_bit];
		end
	end
	
	// Write address pointer
	always_ff @(posedge clk) begin
		if (!reset_n) begin
			write_pointer <= 0;
		end else begin
			if (wren && write_pointer < 3*`TBLEN-1) begin
				write_pointer <= write_pointer + 1;
			end else if (wren) begin
				write_pointer <= 0;
			end
		end
	end
	
	// Write decision bits
	always_ff @(posedge clk) begin
		if (!reset_n) begin
			wren <= 0;
		end else begin
			dec_write_buffer[state_counter] <= dec[1];
			dec_write_buffer[state_counter | (1<<(`PARAM_K-2))] <= dec[0];
			
			if (last_state && do_write)
				wren <= 1'b1;
			else 
				wren <= 1'b0;
		end
	end
	
	// State machine
	always_comb begin
		case (smu_state)
			IDLE:	begin
				if (valid) 
					smu_state_next = WRITE_1;
				else 
					smu_state_next = IDLE;
			end
			WRITE_1: begin
				if (block_complete && last_state)
					smu_state_next = WRITE_2;
				else 
					smu_state_next = WRITE_1;
			end
			WRITE_2: begin	
				if (block_complete && last_state)
					smu_state_next = WTRACEBACK;
				else
					smu_state_next = WRITE_2;
			end
			WTRACEBACK:	begin
				if (tb_decode_complete && !stall_3)
					smu_state_next = WDECODE;
				else 
					smu_state_next = WTRACEBACK;
			end
			WDECODE: begin
				if (tb_decode_complete && !stall_3)
					smu_state_next = WIDLE;
				else
					smu_state_next = WDECODE;
			end
			WIDLE: begin
				if (block_complete && last_state && valid) // write finished
					smu_state_next = WTRACEBACK;
				else if (block_complete && last_state && !valid)
					smu_state_next = TRACEBACK_ONLY;
				else
					smu_state_next = WIDLE;
			end
			TRACEBACK_ONLY: begin	
				if (tb_decode_complete && !stall_3)
					smu_state_next = DECODE_ONLY;
				else
					smu_state_next = TRACEBACK_ONLY;
			end
			DECODE_ONLY: begin
				if (tb_decode_complete && !stall_3)
					smu_state_next = DECODE_FINISH;
				else 
					smu_state_next = DECODE_ONLY;
			end
			DECODE_FINISH: begin
				if (tb_decode_complete  && !stall_3)
					smu_state_next = IDLE;
				else 
					smu_state_next = DECODE_FINISH;
			end
		endcase
	end
	
	always_comb begin
		// State machine outputs
		case(smu_state)
			WRITE_1, WRITE_2, WTRACEBACK, WDECODE, WIDLE: do_write = 1'b1;
			default: do_write = 1'b0;
		endcase
		
		if (smu_state == WTRACEBACK || smu_state == TRACEBACK_ONLY)
			do_traceback = 1'b1;
		else 
			do_traceback = 1'b0;
			
		if (smu_state == WDECODE || smu_state == DECODE_ONLY || smu_state == DECODE_FINISH)
			do_decode = 1'b1;
		else 
			do_decode = 1'b0;	
		
		if (smu_state == DECODE_FINISH)
			finish = 1'b1;
		else
			finish = 1'b0;
			
		if (smu_state == IDLE)
			idle = 1'b1;
		else
			idle = 1'b0;
	end	
	
	always_ff @(posedge clk) begin
		if (!reset_n) begin // reset
			smu_state <= IDLE;
		end else begin	
			smu_state <= smu_state_next;
		end
	end
	
	
endmodule