`include "common.sv"

module input_buffer 
	(
		input logic clk,
		input logic reset_n,
		input logic [7:0] data,
		input logic wren,
		input logic [$clog2(`INPUT_LEN / 8)-1:0] wraddress,
		input logic enable,
		input logic next_symbol,
		output logic valid_out,
		output logic [1:0] data_out
	);
	
	
	logic [$clog2(`INPUT_LEN / 2)-1:0] rdaddress;
	logic [$clog2(`INPUT_LEN / 2)-1:0] counter;
	logic [1:0] out;

	sync_ram_mixed #(
		.DATA_WIDTH_READ  (2), 
		.DATA_WIDTH_WRITE (8),
		.NUMBITS 			(240) // Needs to be divisible by 8 and by TBLEN
		) input_mem (
			.clk			(clk),
			.data			(data),
			.rdaddress 	(rdaddress),
			.wraddress	(wraddress),
			.wren			(wren),
			.q				(out)
		);
		
	always_comb begin
		rdaddress = counter;
	end
	
	always_ff @(posedge clk) begin
		if (!reset_n) begin
			counter <= 0;
			valid_out <= 1'b0;
		end else if ((counter == `INPUT_LEN / 2) && next_symbol) begin
			valid_out <= 1'b0;
		end else if (next_symbol && enable) begin
			counter <= counter + 1;
			data_out <= out;
			valid_out <= 1'b1;
		end
	end

endmodule