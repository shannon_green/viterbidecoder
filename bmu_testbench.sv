`timescale 1ns/1ps
`include "common.sv"

/* Simple testbench for branch metric unit (BMU)
** 
**/
module bmu_testbench();
	logic [1:0] symbol;
	logic [5:0] state;
	logic [1:0] distance;
	logic [1:0] distance1;
	logic clk;

	bmu #({7'o171, 7'o133}, 0) U0 ( 
		.clk				(clk),
		.symbol			(symbol), 
		.state			(state), 
		.out		(distance) 
	); 
	
	bmu #({7'o171, 7'o133}, 1) U1 ( 
		.clk				(clk),
		.symbol			(symbol), 
		.state			(state), 
		.out		(distance1) 
	); 
	 
	initial begin 
		clk = 0;
		symbol = 2'b10;
		state = 6'b010001;
		
		#100 clk = 1;
		#100 clk = 0;
		
		//assert(distance1 == 0);
		$monitor("distance1 = %x", distance1);
		
//		state = 6'b010000;
//		
//		#100 clk = 1;
//		#100 clk = 0;
//		
//		assert(distance1 == 2);
//		$monitor("distance1 = %x", distance1);
//		
//		state = 6'b010001;
//		
//		#100 clk = 1;
//		#100 clk = 0;
//		
//		assert(distance1 == 0);
//		$monitor("distance1 = %x", distance1);
//		
//		$finish;
	end 

endmodule