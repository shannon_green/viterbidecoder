/* Generic synchronous RAM module with one read and one write port.
 * Inputs data, rdaddress, wraddress, wren are registered.
 * Output q is registered.
 * Read-while-write behaviour is undefined.
 */
module sync_ram #(
		parameter DATA_WIDTH = 16,
		parameter NUMWORDS = 64
		)
	(
		input logic clk,
		input logic [DATA_WIDTH-1:0] data,
		input logic [$clog2(NUMWORDS)-1:0] rdaddress,
		input logic [$clog2(NUMWORDS)-1:0] wraddress,
		input logic wren,
		output logic [DATA_WIDTH-1:0] q
	);
	
	// Memory
	logic [DATA_WIDTH-1:0] mem [0:NUMWORDS-1] /* synthesis ramstyle = "no_rw_check" */; 
	
	// Internal signals
	logic [$clog2(NUMWORDS)-1:0] rdaddress_d;
	
	// Register read address input
	always_ff @(posedge clk) begin
		rdaddress_d <= rdaddress;
	end
	
	// Read/write data
	always_ff @(posedge clk) begin
		if (wren) begin
			mem[wraddress] <= data;
		end
		
		//if (wren && wraddress == rdaddress_d) begin
			// Don't care about read-while-write behaviour, 
			// so we specify this explicitly. 
			//q <= {DATA_WIDTH{1'bx}};
		//end else begin
			q <= mem[rdaddress_d];
		//end
	end
	
endmodule