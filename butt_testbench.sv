`timescale 1ns/1ps
`include "common.sv"

/* Simple testbench for butterfly unit
** 
**/
module butt_testbench();
	logic clk;
	logic valid_in;
	logic [`PARAM_R-1:0] symbol;
	logic [`WIDTH_STATE-1:0] state0;
	logic [`WIDTH_STATE-1:0] state1;
	logic [`WIDTH_PM-1:0] oldsm0;
	logic [`WIDTH_PM-1:0] oldsm1;
	logic [`WIDTH_PM-1:0] newsm0;
	logic [`WIDTH_PM-1:0] newsm1;
	logic [1:0] dec;
	logic valid_out;

	butterfly U0 (
		.clk			(clk),
		.valid_in	(valid_in),
		.symbol		(symbol),
		.state0		(state0),
		.state1		(state1),
		.oldsm0		(oldsm0),
		.oldsm1		(oldsm1),
		.newsm0		(newsm0),
		.newsm1		(newsm1),
		.dec			(dec),
		.valid_out	(valid_out)
	); 
	 
	initial begin 
		// Test case 1
		clk = 0;
		symbol = 2'b11;
		valid_in = 1;
		state0 = 0;
		state1 = 1;
		oldsm0 = 3;
		oldsm1 = 9;
		
		// Clock data through
		#100 clk = 1;
		#100 clk = 0;
				
		// Test case 2
		symbol = 2'b10;
		valid_in = 1;
		state0 = 16;
		state1 = 17;
		oldsm0 = 4;
		oldsm1 = 3;

		// Clock data through
		#100 clk = 1;
		#100 clk = 0;
		
		valid_in = 0;
		
		// Check case 1
		assert(newsm0 == 5);
		assert(newsm1 == 3);
		assert(dec == 2'b00);
		assert(valid_out == 1);
		
		// Clock data through
		#100 clk = 1;
		#100 clk = 0;
		
		// Check case 2
		assert(newsm0 == 4);
		assert(newsm1 == 4);
		assert(dec == 2'b11);
		assert(valid_out == 1);
		
		// Clock data through
		#100 clk = 1;
		#100 clk = 0;
	
		assert(valid_out == 0);
		
		// Clock data through
		#100 clk = 1;
		#100 clk = 0;
		
	end 


endmodule