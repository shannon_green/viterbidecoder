`include "common.sv"

/* Top level module for the Viterbi decoder.
 * This module operates as a memory mapped slave 
 * on the Avalon bus.
 * Shannon Green 2015
 * me@shannon.green
 */
module ViterbiDecoder(
	input logic clk,
	input logic reset_n,
	input logic chipselect,
	input logic write,
	input logic read, 
	input logic [11:0] address,
	input logic [7:0] writedata, 
	output logic [7:0] readdata
);

	logic [7:0] input_data;
	logic input_wren;
	logic [$clog2(`INPUT_LEN / 8)-1:0] input_wraddress;
	logic input_enable;
	logic next_symbol;
	logic input_valid_out;
	logic [1:0] input_data_out;
	
	logic processor_out;
	logic processor_valid_out;
	
	logic [7:0] data_out;
	logic data_ready;
	
	logic combined_reset;
	logic sreset = 1'b0;
	
	logic [$clog2(`INPUT_LEN / 2 / 8)-1:0] lifo_rdaddress;
	
	always_comb begin
		combined_reset = reset_n & ~sreset;
	end
	
	/* Top-level address decoder
	**	0x0XX: 	Write input bits up to INPUT_LEN
	**				Read undefined
	**	0x4XX: 	Write undefined
	**				Read output bits (if data_ready)
	**	0x8XX:	Write (anything) to start decoder
	**				Read data_ready
	**	0xCXX:	Write to clear decoder state
	**				Read undefined
	*/
	
	always_ff @(posedge clk) begin: write_address_decoder
		if (!reset_n) begin
			sreset <= 1'b0;
		end else if (write && chipselect) begin
			case (address)
				{2'h0, address[9:0]}: begin
					// Writing data to input buffer
					input_data <= writedata;
					input_wraddress <= address[$clog2(`INPUT_LEN / 8)-1:0];
					input_wren <= 1'b1;
					input_enable <= 1'b0;
				end
				
				{2'h2, address[9:0]}: begin
					// Start decoder
					input_data <= 0;
					input_wraddress <= 0;
					input_wren <= 1'b0;
					input_enable <= 1'b1;
				end
			
				default: begin
					// Any other address: clear
					input_data <= 0;
					input_wraddress <= 0;
					input_wren <= 1'b0;
					input_enable <= 1'b0;
					sreset <= 1'b1;
				end
			endcase
		end else begin
			sreset <= 1'b0;
		end
	end
	
	always_ff @(posedge clk) begin: read_address_decoder
		if (read && chipselect) begin
			case (address)
				{2'h1, address[9:0]}: begin
					// Reading from output buffer
					readdata <= data_out;
					lifo_rdaddress <= address[$clog2(`INPUT_LEN / 2 / 8)-1:0];
				end
				
				{2'h2, address[9:0]}: begin
					// Return nonzero if data ready
					readdata <= {8{{data_ready}}};
					lifo_rdaddress <= address[$clog2(`INPUT_LEN / 2 / 8)-1:0];
				end
				
				default: begin
					readdata <= 69;
					lifo_rdaddress <= 0;
				end
			endcase
		end
	end


	input_buffer in_buffer (
		.clk				(clk),
		.reset_n			(combined_reset),
		.data				(input_data),
		.wren				(input_wren),
		.wraddress		(input_wraddress),
		.enable			(input_enable),
		.next_symbol	(next_symbol),
		.valid_out		(input_valid_out),
		.data_out		(input_data_out)
	);

	processor viterbi_processor (
		.clk				(clk),
		.reset_n			(combined_reset),
		.symbol			(input_data_out),
		.valid_in		(input_valid_out),
		.out				(processor_out),
		.next_symbol	(next_symbol),
		.valid_out		(processor_valid_out)
	);
	
	lifo_buffer lifo (
		.clk				(clk),
		.reset_n			(combined_reset),
		.data				(processor_out),
		.en				(processor_valid_out),
		.rdaddress		(lifo_rdaddress),
		.data_out		(data_out),
		.data_ready		(data_ready)
	);
	
endmodule