 `include "common.sv"

module lifo_buffer 
	(
		input logic clk,
		input logic reset_n,
		input logic data,
		input logic en,
		input logic [$clog2(`INPUT_LEN / 2 / 8)-1:0] rdaddress,
		output logic [7:0] data_out,
		output logic data_ready
	);
	
	
	logic [$clog2(`INPUT_LEN / 2)-1:0] wraddress;
	logic [$clog2(`INPUT_LEN / 2)-1:0] counter;
	logic [$clog2(`INPUT_LEN / 2)-1:0] block_base;
	
	sync_ram_mixed #(
		.DATA_WIDTH_READ  (8), 
		.DATA_WIDTH_WRITE (1),
		.NUMBITS 			(120) // Needs to be divisible by 8 and by TBLEN
		) lifo_mem (
			.clk			(clk),
			.data			(data),
			.rdaddress 	(rdaddress),
			.wraddress	(wraddress),
			.wren			(en),
			.q				(data_out)
		);
		
	always_comb begin
		wraddress = block_base + counter;
	end
	
	always_ff @(posedge clk) begin
		if (!reset_n) begin
			counter <= `TBLEN - 1;
			block_base <= 0;
			data_ready <= 0;
		end else if (en && counter == 0) begin
			counter <= `TBLEN - 1;
			block_base <= block_base + `TBLEN;
		end else if (en) begin
			counter <= counter - 1;
		end else begin // en == 0
			if (block_base == (`INPUT_LEN/2)) 
				data_ready <= 1'b1;
		end

	end

	
	
endmodule