`include "common.sv"

/* Branch metric unit (BMU)
** Parameters: GENERATORS - generator polynomials for the convolutional code.
** Inputs: symbol, state, path_select
** Outputs: distance
**/
module bmu 
	#(
		parameter bit [`PARAM_K-1:0] GENERATORS [`PARAM_R-1 : 0] = '{7'o171, 7'o133},
		parameter bit PATH_SELECT = 0
	)
	(
		input logic clk,
		input logic [`PARAM_R-1:0] symbol,
		input logic [`WIDTH_STATE-1:0] state,
		output logic [`WIDTH_BM-1:0] out
	);

	logic [`PARAM_R-1:0] path;
	logic [`PARAM_K-1:0] conv_state;
	logic [`PARAM_R-1:0] diff;
	logic [`WIDTH_BM-1:0] distance; 
	
	int i;
	int j;
	
	always_comb begin	
		// Compute the edge weights
		for (j = 0; j < `PARAM_R; j=j+1) begin: conv_poly
			// Find the product of the generator polynomials
			// with the current state 
			conv_state = {PATH_SELECT, state} & GENERATORS[j];
			// Sum the bits modulo 2 (xor)
			path[j] = (^conv_state);
		end
		
		// Compute the hamming distance between the
		// calculated edge weight and the input symbol
		diff = path ^ symbol;
		distance = 0;
		for (i = 0; i < `PARAM_R; i=i+1) begin: adder_tree
			distance += diff[i];
		end
	end
	
	
	// Push out to pipeline
	always_ff @(posedge clk) begin
		out <= distance;
	end

endmodule


